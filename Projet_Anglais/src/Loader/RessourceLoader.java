package Loader;


import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

public class RessourceLoader {
	public static InputStream load(String path){
		InputStream input = RessourceLoader.class.getResourceAsStream(path);
		if(input == null){
			input = RessourceLoader.class.getResourceAsStream("/"+path);
		}
		return input;
	}
	
	
	public static BufferedImage icone(){
		BufferedImage icone = null;
		try {
			icone = ImageIO.read(RessourceLoader.load("/icone.png"));
		} catch (IOException e) {}
		return icone;
	}
}
