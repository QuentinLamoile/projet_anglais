package Vue;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import Learning.ExerciceTexteTrou;
import Learning.ExerciceVoca;
import Learning.LMachine;
import Loader.RessourceLoader;
import Vue.ExTexteTrou.ModeleTexteTrou;
import Vue.ExVoca.ModeleVoca;
import Vue.ExVoca.RenduCellule;
import Vue.ExVoca.VuCorrige;

public class MainVue extends JFrame{
	
	private static final long serialVersionUID = 1L;
	
	private LMachine machine;
	
	public static ModeleVoca modeleatrou = new ModeleVoca(ExerciceVoca.listMotTrouRemplissage);
	
	//tableau de fichier
	private int indice = 0;
	
	//police
	Font policeTeste = new Font("Arial", Font.PLAIN, 12);
	Font policeConsigne = new Font("Arial",Font.BOLD,14);
	Font policeWelcome = new Font("Arial",Font.BOLD,80);

    //taille Ecran appli
    public static int largeur = 800;
    public static int hauteur = 600;
    
    //taille ecran machine
    public static Dimension scrennSize = Toolkit.getDefaultToolkit().getScreenSize();
    
    //CardLayout
    private JPanel exercice =  new JPanel();
	private CardLayout c = new CardLayout();
	private String[] listContent = {"CARD_0","CARD_1", "CARD_2"};
	
	//Message de bienvenue
	private JLabel msgBienvenue = new JLabel("<html><center>Welcome<br>to Learning Machine</center></html>");
    
    //Element de la vue
    private JPanel principal = new JPanel();
	private JPanel bas = new JPanel(new FlowLayout(FlowLayout.RIGHT)); //Le JPanel est dipos� en bas a droite
	
	private JPanel exerciceVoca = new JPanel();
	private JTable tableau = new JTable(modeleatrou);
	
	private JPanel exerciceTexteTrou = new JPanel();
	
	private JButton check = new JButton("Check");
	private JButton corrige = new JButton("Correct") ;
	
	private JMenuBar menuBar = new JMenuBar();
	
	private JMenu fichier = new JMenu("File");
	private JMenu choixExercice = new JMenu("Choose exercise");
	private JMenu aide = new JMenu("Help");
	
	//JMenu fichier
	private JMenuItem ouvrir = new JMenuItem("Open");
	private static JMenuItem enregistrer = new JMenuItem("Save");
	private JMenuItem quitter = new JMenuItem("Exit");
	
	//JMenu choixExercice
	private JMenu exVoca = new JMenu("Vocab Exercise");
	private JMenu exTexteTrou = new JMenu("Gaps fills");
	
	//JMenu aide
	private JMenuItem help = new JMenuItem("Help Contents");
	private JMenuItem contact = new JMenuItem("Contact Us");
	private JMenuItem aPropos = new JMenuItem("About Learning Machine");
	
	public static JMenuItem getEnregistrer() {
		return enregistrer;
	}

	/*************************
	 * Contructeur de la vue 
	 * @param exercice *
	 *************************/
    public MainVue(ArrayList<String> exercice) {
    	initialisationVue();
    	
    	
    	/*
    	 * Action sur Fichier
    	 */
    	//Action sur Ouvrir
    	ouvrir.addActionListener(new ActionListener() {
    		@Override
    		public void actionPerformed(ActionEvent e) {
    			int reponce = verificationEnregistrement();
    			if(reponce == 1){
    				File repertoireCourant = null;
    				try {
    					// obtention d'un objet File qui d�signe le r�pertoire courant.
    					repertoireCourant = new File(".").getCanonicalFile();
    					System.out.println("R�pertoire courant : " + repertoireCourant);
    				} catch(IOException e1) {}

    				// cr�ation de la bo�te de dialogue dans ce r�pertoire courant
    				// (ou dans "home" s'il y a eu une erreur d'entr�e/sortie, auquel
    				// cas repertoireCourant vaut null)
    				JFileChooser dialogue = new JFileChooser(repertoireCourant);

    				// affichage
    				int retour = dialogue.showOpenDialog(principal);


    				if(retour == JFileChooser.APPROVE_OPTION) {
    					// r�cup�ration du fichier s�lectionn�
    					String nomFichier = dialogue.getSelectedFile().getName();
    					System.out.println("Fichier choisi : " + nomFichier);

    					//ouverture du fichier en faisant le choix du bon exercice
    					//ouverture fichier type .voca
    					if(nomFichier.endsWith(".voca")){
    						
    						try {
    							machine = new ExerciceVoca();
    							machine.ouvrir(nomFichier);
    							affichageExVoca();

    						} catch (IOException e1) {
    							e1.printStackTrace();
    						}
    						//ouverture de fichier type .trou
    					}else if (nomFichier.endsWith(".trou")) {
    						try {
    							machine = new ExerciceTexteTrou();
    							machine.ouvrir(nomFichier);
    							affichageExTexteTrou();

    						} catch (IOException e1) {
    							e1.printStackTrace();
    						}

    					}else {
    						JOptionPane.showMessageDialog(principal, "You have chosen an incorrect file type\n "
    								+ "Select an file type *.voca or *.trou", "Error", JOptionPane.ERROR_MESSAGE);
    					}
    				} 
    			}else if(reponce == 0){
    				enregistrer();
    			}

    		}
    	});
    	
    	//Action sur Enregistrer
    	enregistrer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//coder en fin de fichier car elle est utiliser autre part
				enregistrer();
			}
		});
    	
    	//Action sur Quitter
    	quitter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int reponce = verificationEnregistrement();
				if(reponce == 1){
					System.exit(0);
				}else if(reponce == 0){
					enregistrer();
				}
			}
		});
    	
    	/*
    	 * Action sur les choix
    	 */
    	//Action Exercice de Voca
    	for( indice=0 ; indice<exercice.size() ; indice++){
    		if(exercice.get(indice).endsWith(".voca")){

    			exVoca.add(new JMenuItem(exercice.get(indice).substring(0,exercice.get(indice).length()-5))).addActionListener(new ActionListener() {
    				int indice = MainVue.this.indice;
    				@Override
    				public void actionPerformed(ActionEvent e) {
    					int reponce = verificationEnregistrement();
    					if(reponce == 1){
    						try {
    							machine = new ExerciceVoca();
    							machine.ouvrir("/"+exercice.get(indice));
    							affichageExVoca();
    						} catch (IOException e1) {
    							e1.printStackTrace();
    						}	
    					}else if(reponce == 0){
    						enregistrer();
    					}

    				}
    			});
    		}
    	}
    	
    	//Action Exercice de Texte� Trou
    	for( indice=0 ; indice<exercice.size() ; indice++){
    		if(exercice.get(indice).endsWith(".trou")){

    			exTexteTrou.add(new JMenuItem(exercice.get(indice).substring(0,exercice.get(indice).length()-5))).addActionListener(new ActionListener() {
    				int indice = MainVue.this.indice;
    				@Override
    				public void actionPerformed(ActionEvent e) {
    					int reponce = verificationEnregistrement();
    					if(reponce == 1){
    						try {
    							machine = new ExerciceTexteTrou();
    							machine.ouvrir("/"+exercice.get(indice));
    							affichageExTexteTrou();
    						} catch (IOException e1) {
    							e1.printStackTrace();
    						}	
    					}else if(reponce == 0){
    						enregistrer();
    					}

    				}
    			});
    		}
    	}

    	/*
    	 * Action du JButton Check
    	 */
    	check.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(machine instanceof ExerciceVoca)ModeleVoca.validationCellule(tableau);
				if(machine.check()){
					JOptionPane.showMessageDialog(principal, "Congratulation you are passed an exercise", "Information", JOptionPane.INFORMATION_MESSAGE);
				}else{
					JOptionPane.showMessageDialog(principal, "Error(s) in your exercise", "Information",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
    	
    	
    	/*
    	 * Action du JButton Corrige
    	 */
    	corrige.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new VuCorrige();
			}
		});
    	
    	
    	/*
    	 * help
    	 */
    	help.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(principal, "Welcome to Learning Machines' help.\n"
						+ "\n"
						+ "This application allows you to learn English.\n"
						+ "To begin, choose an exercise with Choose exercise menu. You have two choices: Gaps fills and Vocab exercise.\n"
						+ "Vocab exercise, enables you to search for words in English or French depending on the starting word. (The program choose randomly).\n"
						+ "The gaps fills exercise gives you a word list and you have to fill the gaps with this words.\n"
						+ "\n"
						+ "You can save your work, at any time with the file menu, Save or the shortcut Ctrl+S.\n"
						+ "You can open your previous work with the menu File, Open or the shortcut Ctrl+O", "Help Contents", JOptionPane.INFORMATION_MESSAGE);
			}
		});    	
    	
    	/*
    	 * contact
    	 */
    	JEditorPane mail = new JEditorPane("text/html","<html>You can contact :<br>"
				+ "Quentin : <a href=\"mailto:quentin.lamoile@telecomnancy.net\">quentin.lamoile@telecomnancy.net</a><br>"
				+ "Adrien : <a href=\"mailto:adrien.pair@telecomnancy.net\">adrien.pair@telecomnancy.net</a></html>");
    	mail.setEditable(false);
    	mail.setBackground(principal.getBackground());
    	mail.addHyperlinkListener(new HyperlinkListener()
        {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent e)
            {
                if (e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED)){
                	Desktop d = Desktop.getDesktop();
                	try {
						d.browse(new URI(e.getURL().toString()));
					} catch (IOException e1) {
						e1.printStackTrace();
					} catch (URISyntaxException e1) {
						e1.printStackTrace();
					}
                }
            }
        });
    	
    	contact.addActionListener(new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(principal, mail,"Contact us",JOptionPane.INFORMATION_MESSAGE);
			}
		});
    	
    	/*
    	 * A propos
    	 */
    	aPropos.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(principal, "Learning machine is an application to learn English for the French people.\n"
						+ "It�s a project created by Quentin LAMOILE and Adrien PAIR in March 2015.\n", "Information", JOptionPane.INFORMATION_MESSAGE);
			}
		});
    }
    
    /****************************
     * Initialisation de la Vue *
     ****************************/
    public void initialisationVue(){
    	/*
    	 * Look Windows
    	 */
    	try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (ClassNotFoundException e2) {
			e2.printStackTrace();
		} catch (InstantiationException e2) {
			e2.printStackTrace();
		} catch (IllegalAccessException e2) {
			e2.printStackTrace();
		} catch (UnsupportedLookAndFeelException e2) {
			e2.printStackTrace();
		}
    	
    	//Initialisation de CardLayout
    	exercice.setLayout(c);
    	exercice.add(msgBienvenue, listContent[0]);
    	exercice.add(exerciceVoca, listContent[1]);
    	exercice.add(exerciceTexteTrou, listContent[2]);
    	
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	this.setContentPane(principal);
    	this.setTitle("Learning Machine");
    	this.setSize(largeur,hauteur);
    	this.setVisible(true);
    	this.setLocation((scrennSize.width-largeur)/2, (scrennSize.height-hauteur)/2);
    	this.setResizable(false);
    	this.setJMenuBar(menuBar);
    	principal.setLayout(new BorderLayout());
    	principal.add(exercice, BorderLayout.CENTER);
    	principal.add(bas,BorderLayout.SOUTH);

    	//icon
    	this.setIconImage(RessourceLoader.icone());
    	
    	//message de bienvenue
    	msgBienvenue.setFont(policeWelcome);
    	c.show(exercice, listContent[0]);
    	
    	//Barre Menu
    	menuBar.add(fichier);
    	menuBar.add(choixExercice);
    	menuBar.add(aide);
    	
    	//Menu Fichier
    	fichier.add(ouvrir);
    	fichier.add(enregistrer);
    	fichier.addSeparator();
    	fichier.add(quitter);
    	
    	//Menu Choix Exercice
    	choixExercice.add(exVoca);
    	choixExercice.add(exTexteTrou);
    	
    	//Menu aide
    	aide.add(help);
    	aide.add(contact);
    	aide.addSeparator();
    	aide.add(aPropos);
    	
    	//Ajout du button check
    	bas.add(corrige);
    	bas.add(check);
    	
    	//raccourcis clavier
    	ouvrir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
    	enregistrer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));    	
    	quitter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE,ActionEvent.SHIFT_MASK));
    	
    	//Visibilit� � l'ouverture de l'application
    	enregistrer.setEnabled(false);
    	corrige.setVisible(false);
    	check.setVisible(false);
    }
    
    /**************************************************
     * Enregistre dans le bon format celon l'exercice *
     **************************************************/
    public void enregistrer(){
    	File repertoireCourant = null;
        try {
            // obtention d'un objet File qui d�signe le r�pertoire courant.
            repertoireCourant = new File(".").getCanonicalFile();
            System.out.println("R�pertoire courant : " + repertoireCourant);
        } catch(IOException e1) {}
         
        // cr�ation de la bo�te de dialogue dans ce r�pertoire courant
        // (ou dans "home" s'il y a eu une erreur d'entr�e/sortie, auquel
        // cas repertoireCourant vaut null)
        JFileChooser dialogue = new JFileChooser(repertoireCourant);
         
        // affichage
        int retour = dialogue.showSaveDialog(principal);
        if(retour == JFileChooser.APPROVE_OPTION) {
	        //r�cup�ration du nom du fichier
	        String nomFichier = dialogue.getSelectedFile().getName();
	        System.out.println("Nom fichier : " + nomFichier);
	        
	        //Enregistrement avec la bonne extension
	        try {
	        	if(machine instanceof ExerciceVoca)ModeleVoca.validationCellule(tableau);
	        	
				machine.enregistrer(nomFichier);
				
		        //visibilit�
		        enregistrer.setEnabled(false);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
        }else {
			JOptionPane.showMessageDialog(principal, "Save Erreur", "Error", JOptionPane.ERROR_MESSAGE);
		}
    }
    
    /*******************************************
     * V�rification pour pas perdre les donn�es *
     * retourne 1 si pas besoin d'enregistrer  *
     * retourne 0 si enregistrement demander   *
     *******************************************/
    public int verificationEnregistrement(){
    	//System.out.println(principal.isAncestorOf(excercice));
		if((principal.isAncestorOf(exerciceVoca)||principal.isAncestorOf(exerciceTexteTrou))&&enregistrer.isEnabled()){
			return JOptionPane.showConfirmDialog(principal, "Warning the data will be lost !\n"
					+ "Do you want to save your work ?", "Warning", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
		}else return 1;
    }
    
    /***********************************************
     * affichage du panel pour l'excercice de Voca *
     ***********************************************/
    public void affichageExVoca(){
    	exerciceVoca.removeAll();
    	exerciceVoca.setLayout(new BorderLayout());
		tableau.getColumnModel().getColumn(0).setCellRenderer(new RenduCellule());
		tableau.getColumnModel().getColumn(1).setCellRenderer(new RenduCellule());
		tableau.addKeyListener(new EvenementClavier());
		exerciceVoca.add(new JScrollPane(tableau));
		c.show(exercice, listContent[1]);
		principal.validate();
        
        //visibilit� 
		enregistrer.setEnabled(true);
		corrige.setVisible(true);
		check.setVisible(true);
    }
    
    /******************************************************
     * Affichage du panel pour l'exercice de texte � trou *
     ******************************************************/
    public void affichageExTexteTrou(){
    	//nettoyage
    	exerciceTexteTrou.removeAll();
    	ModeleTexteTrou.jTextField.clear();
    
    	exerciceTexteTrou.setLayout(new BorderLayout());
    	
    	//Cr�ation des box
    	Box boxHori = Box.createHorizontalBox();
    	Box boxVert = Box.createVerticalBox();
    	
    	//insertion de la consigne
    	String consigne = "Fill the gaps with the words in this list : ";
    	ModeleTexteTrou.rempliArray();
    	System.out.println(ModeleTexteTrou.tabMotATrouver.size());
    	while(!ModeleTexteTrou.tabMotATrouver.isEmpty()){
    		int indice = ModeleTexteTrou.indiceRamdom(ModeleTexteTrou.tabMotATrouver.size());
    		if(ModeleTexteTrou.tabMotATrouver.size()==1){
    			consigne = consigne + ModeleTexteTrou.tabMotATrouver.get(0);
    			ModeleTexteTrou.tabMotATrouver.remove(0);
    		}else{
    			consigne = consigne + ModeleTexteTrou.tabMotATrouver.get(indice)+" / ";
    			ModeleTexteTrou.tabMotATrouver.remove(indice);
    		}
    	}
    	JLabel labelConsigne = new JLabel(consigne);
    	labelConsigne.setFont(policeConsigne);
    	boxHori.add(labelConsigne);
    	boxHori.setAlignmentX(0);
    	boxVert.add(boxHori);
    	
    	//ajout d'une ligne apr�s la consigne
    	boxVert.add(Box.createHorizontalBox().add(new JLabel(" ")));
    	boxHori = Box.createHorizontalBox();
    	
    	//insertion des labels et JTextefiel 
		for(int i=0 ; i<ExerciceTexteTrou.texteTrou.size() ; i++){
			JLabel label = new JLabel(ExerciceTexteTrou.texteTrou.get(i).getTexte());
			label.setFont(policeTeste);
			boxHori.add(label);
			
			//On ajoute un JTextField que si ExerciceTexteTrou.texteTrou.get(i).getMotATrouver().equals(null)
			if(ExerciceTexteTrou.texteTrou.get(i).getMotATrouver()!=null){
				JTextField test = new JTextField(ExerciceTexteTrou.texteTrou.get(i).getMotATrouver());
				test.setPreferredSize(new Dimension(150, 25));
				test.setMaximumSize(test.getPreferredSize());
				test.addKeyListener(new EvenementClavier());
				ModeleTexteTrou.jTextField.add(test);
				boxHori.add(test);
			}else{
				boxHori.setAlignmentX(0);
				boxVert.add(boxHori);
				
				boxHori = Box.createHorizontalBox();
			}
		}
		
		exerciceTexteTrou.add(new JScrollPane(boxVert));
		c.show(exercice, listContent[2]);
		principal.validate();
		
		//visibilit� 
		enregistrer.setEnabled(true);
		corrige.setVisible(false);
		check.setVisible(true);
    }
    
}
