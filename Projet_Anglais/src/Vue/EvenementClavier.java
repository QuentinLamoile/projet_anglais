package Vue;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class EvenementClavier implements KeyListener{

	@Override
	public void keyPressed(KeyEvent e) {
		MainVue.getEnregistrer().setEnabled(true);
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		MainVue.getEnregistrer().setEnabled(true);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		MainVue.getEnregistrer().setEnabled(true);
	}
}