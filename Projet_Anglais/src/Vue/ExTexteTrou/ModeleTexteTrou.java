package Vue.ExTexteTrou;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JTextField;

import Learning.ExerciceTexteTrou;

public class ModeleTexteTrou {
	public static ArrayList<JTextField> jTextField = new ArrayList<JTextField>();
	public static ArrayList<String> tabMotATrouver = new ArrayList<String>();
	
	//rempli le tableau texteTrou des r�ponces du l'utilisateur et met la couleur
	public static void recuperationTexte(){
    	int indiceJtext = 0;
    	for(int i=0 ; i<ExerciceTexteTrou.texteTrou.size() ; i++){
    		if(ExerciceTexteTrou.texteTrou.get(i).getMotATrouver()!=null){
    			ExerciceTexteTrou.texteTrou.get(i).setMotATrouver(jTextField.get(indiceJtext).getText());
    			if(jTextField.get(indiceJtext).getText().equals(ExerciceTexteTrou.texteReponse.get(i).getMotATrouver())){
    				jTextField.get(indiceJtext).setBackground(Color.GREEN);
    			}else jTextField.get(indiceJtext).setBackground(Color.RED);
    			indiceJtext++;
    		}
    	}
    }
	
	//rempli le tableau tabMotATrouver des r�ponces que l'on doit afficher dans la consigne
	public static void rempliArray(){
		tabMotATrouver.clear();
    	for(int i=0 ; i<ExerciceTexteTrou.texteReponse.size() ; i++){
    		if(ExerciceTexteTrou.texteReponse.get(i).getMotATrouver()!=null){
	    		tabMotATrouver.add(ExerciceTexteTrou.texteReponse.get(i).getMotATrouver());
    		}
    	}
	}
	
	//effectue un ramdom sur un chiffre compris en 0 et la taille du tableau
	public static int indiceRamdom(int tailleTab){
		return new Random().nextInt(tailleTab);
	}
	
	
	
}
