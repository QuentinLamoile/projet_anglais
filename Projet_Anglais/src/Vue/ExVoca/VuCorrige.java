package Vue.ExVoca;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import Learning.ExerciceVoca;
import Loader.RessourceLoader;

public class VuCorrige extends JFrame{

	private static final long serialVersionUID = 1L;
	
	private JFrame f = new JFrame();

	//Element de la vue
    private JPanel principal = new JPanel();
    
    private JPanel corrige =  new JPanel();
    
    private ModeleVoca modelerempli = new ModeleVoca(ExerciceVoca.listMot);
    private JTable tableau = new JTable(modelerempli);
    
    //taille Ecran appli
    public static int largeur = 600;
    public static int hauteur = 400;
    
    //taille ecran machine
    public static Dimension scrennSize = Toolkit.getDefaultToolkit().getScreenSize();
    
    private JPanel bas = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    
    JButton ok = new JButton("OK");
    
    public VuCorrige(){
    	f.setContentPane(principal);
    	f.setTitle("Learning Machine : correction");
    	f.setSize(largeur,hauteur);
    	f.setVisible(true);
    	f.setLocation((scrennSize.width-largeur)/2, (scrennSize.height-hauteur)/2);
    	//this.setResizable(false);
    	
    	f.setIconImage(RessourceLoader.icone());
    	
    	principal.setLayout(new BorderLayout());
    	principal.add(corrige, BorderLayout.CENTER);
    	principal.add(bas,BorderLayout.SOUTH);
    	
    	corrige.setLayout(new BorderLayout());
    	corrige.add(new JScrollPane(tableau));
    	
    	bas.add(ok);
    	
    	ok.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				f.dispose();			
			}
		});
    }
}
