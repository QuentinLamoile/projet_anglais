package Vue.ExVoca;

import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import Learning.ExerciceVoca;
import Learning.Object.Mot;
import Vue.MainVue;

public class ModeleVoca extends AbstractTableModel{

	private static final long serialVersionUID = 1L;

	private ArrayList<Mot> motTable = new ArrayList<Mot>();
	private static ArrayList<Mot> listRecupereTableau = new ArrayList<Mot>();
	 
    private final String[] entetes = {"English", "French"};
 
    public ModeleVoca(ArrayList<Mot> listMot) {
    	this.motTable.clear();
    	this.motTable = listMot;
    }
 
    public int getRowCount() {
        return motTable.size();
    }
 
    public int getColumnCount() {
        return entetes.length;
    }
 
    public String getColumnName(int columnIndex) {
        return entetes[columnIndex];
    }
 
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return motTable.get(rowIndex).getMotAnglais();
            case 1:
                return motTable.get(rowIndex).getMotFrancais();
            default:
                return null; //Ne devrait jamais arriver
        }
    }
    
    public void addAmi(Mot mot) {
    	motTable.add(mot);
        fireTableRowsInserted(motTable.size() -1, motTable.size() -1);
    }
 
    public void removeAmi(int rowIndex) {
    	motTable.remove(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }
    
    //edition des cellules
    public boolean isCellEditable(int rowIndex, int columnIndex) {
    	if(ExerciceVoca.listMot.get(rowIndex).getMotAnglais().equals((String)MainVue.modeleatrou.getValueAt(rowIndex, columnIndex)) ){
    		return false;
    	}else if (ExerciceVoca.listMot.get(rowIndex).getMotFrancais().equals((String)MainVue.modeleatrou.getValueAt(rowIndex, columnIndex)) ) {
			return false;
		}else return true;
    }
    
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if(aValue != null){
            Mot mot = motTable.get(rowIndex);
     
            switch(columnIndex){
                case 0:
                    mot.setMotAnglais((String)aValue);
                    break;
                case 1:
                    mot.setMotFrancais((String)aValue);
                    break;
            }
        }
    }
    
    public static ArrayList<Mot> recuperationTable(){
    	listRecupereTableau.clear();
    	for(int i=0 ; i<MainVue.modeleatrou.getRowCount() ; i++){
    		listRecupereTableau.add(new Mot((String)MainVue.modeleatrou.getValueAt(i, 0), (String)MainVue.modeleatrou.getValueAt(i, 1)));
    	}
    	return listRecupereTableau;
    }
    
    //stopper l'�dition de la cellule
    public static void validationCellule(JTable tableau){
    	if(tableau.isEditing()){
    		tableau.getCellEditor().stopCellEditing();
    	}
    }
    
}
