package Vue.ExVoca;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import Learning.ExerciceVoca;

public class RenduCellule extends DefaultTableCellRenderer{

	private static final long serialVersionUID = 1L;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		super.getTableCellRendererComponent(table, value, isSelected, hasFocus,  row, column);
		String chaineEcrite = (String)value;
		/*System.out.println("chaine ecrite : "+chaineEcrite);
		System.out.println("listMot : "+ExerciceVoca.listMot.get(row).getMotAnglais());
		System.out.println("lisMotTrou : "+ExerciceVoca.listMotTrou.get(row).getMotAnglais());*/
		if(column==0){
			if(ExerciceVoca.listMotTrou.get(row).getMotAnglais().equals("")&&!ExerciceVoca.listMot.get(row).getMotAnglais().equals(chaineEcrite)){
				setBackground(Color.RED);
			}else{
				setForeground(Color.BLUE);
				setBackground(Color.WHITE);
			}
			if(ExerciceVoca.listMotTrou.get(row).getMotAnglais().equals("")&&ExerciceVoca.listMot.get(row).getMotAnglais().equals(chaineEcrite)){
				setBackground(Color.GREEN);
			}else if(ExerciceVoca.listMotTrou.get(row).getMotAnglais().equals("")&&!ExerciceVoca.listMot.get(row).getMotAnglais().equals(chaineEcrite)){
				setForeground(Color.WHITE);
				setBackground(Color.RED);
			}
		}else{
			if(ExerciceVoca.listMotTrou.get(row).getMotFrancais().equals("")&&!ExerciceVoca.listMot.get(row).getMotFrancais().equals(chaineEcrite)){
				setBackground(Color.RED);
			}else{
				setForeground(Color.BLUE);
				setBackground(Color.WHITE);
			}
			if(ExerciceVoca.listMotTrou.get(row).getMotFrancais().equals("")&&ExerciceVoca.listMot.get(row).getMotFrancais().equals(chaineEcrite)){
				setBackground(Color.GREEN);
			}else if(ExerciceVoca.listMotTrou.get(row).getMotFrancais().equals("")&&!ExerciceVoca.listMot.get(row).getMotFrancais().equals(chaineEcrite)){
				setForeground(Color.WHITE);
				setBackground(Color.RED);
			}
		}
		
		return this;
	}
}
