package Learning;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import Learning.Object.Texte;
import Loader.RessourceLoader;
import Vue.ExTexteTrou.ModeleTexteTrou;


public class ExerciceTexteTrou implements LMachine {
	public static ArrayList<Texte> texteReponse = new ArrayList<Texte>();
	/* le texte source avec les bonnes r�ponces*/
	public static ArrayList<Texte> texteTrou = new ArrayList<Texte>();
	/*le texte sans les mot � trouver a l'ouverture d'un fichier source 
	sinon les mots de l'utilisateur rentrer � l'ouverture d'un fichier qu'il a enregistrer*/
	
	@Override
	public void ouvrir(String nomFichier) throws IOException {
		texteReponse.clear() ; //Nettoyage de l'ArrayList � chaque ouverture de fichier
		texteTrou.clear() ; //Nettoyage de l'ArrayList � chaque ouverture de fichier
		
		
		
		/*
		texteReponse.add(new Texte("Robin loves ", "playing"));
		texteTrou.add(new Texte("Robin loves ","uuu"));

		texteReponse.add(new Texte(" tennis, because he ","was"));
		texteTrou.add(new Texte(" tennis, because he ","uuu"));

		texteReponse.add(new Texte(" a tennis player.",null));
		texteTrou.add(new Texte(" a tennis player.",null));*/

		/*	texteReponse.add(new Texte("He is very ","strong"));
		texteTrou.add(new Texte("He is very ",""));

		texteReponse.add(new Texte(", and his father too.\n",null));
		texteTrou.add(new Texte(", and his father too.\n",null));

			for(int i=0; i<texteReponse.size() ; i++) {
			System.out.println("i = "+i+" 11Texte : "+ texteReponse.get(i).getTexte()) ;
			System.out.println("i = "+i+" 11Mot a trouver : " + texteReponse.get(i).getMotATrouver());
		}

		for(int i=0; i<texteTrou.size() ; i++) {
			System.out.println("i = "+i+" 22Texte : "+ texteTrou.get(i).getTexte()) ;
			System.out.println("i = "+i+" 22Mot a trouver : " + texteTrou.get(i).getMotATrouver());
		} */
		

		System.out.println("Ouverture ExerciceTexteTrou");
		System.out.println(nomFichier);
		String sautDeLigne = System.getProperty("line.separator") ;

		//Ouverture fichier et on stocke les mots 
		//BufferedReader in;
		//in = new BufferedReader(new FileReader(nomFichier));
		Scanner in = new Scanner(RessourceLoader.load(nomFichier));
		String line;

		//line = in.readLine() ;
		line = in.nextLine();
		String MotDepart = null ;
		//while ((line = in.readLine()) != null){
		while(in.hasNextLine()){
			line = in.nextLine();
			//Fichier non commenc� et commenc�
			String temp = line.replaceAll(sautDeLigne, null) ;		
			String tab[] = temp.split("=") ;
			//System.out.println("Longueur du tableau = "+tab.length);

			for(int i = 0; i<tab.length ; i++) {
				if(i%2==0) {
					//Les mots donn�s
					//System.out.println("i = "+i+" - Texte de d�part : "+tab[i]);
					MotDepart = tab[i] ;
					texteTrou.add(new Texte(tab[i],"")) ;	
					texteReponse.add(new Texte(MotDepart, ""));
					//System.out.println("Texte : "+ texteTrou.get(i).getTexte()) ;

				} else {
					//Les mot entre =***= (Mot r�ponse)
					System.out.println("i = "+i+" - Mot r�ponse : "+tab[i]);		
					if (tab[i].indexOf("%")!=-1) { //Gestion du cas ou le fichier a d�j� �t� commenc� (pr�sence de % dans les mots r�ponses)
						int premierPourcent = tab[i].indexOf("%") ;
						int deuxiemePourcent = tab[i].indexOf("%",premierPourcent+1) ;
						String extraction = tab[i].substring(premierPourcent+1,deuxiemePourcent) ;
						//System.out.println("Extraction :" + extraction);

						texteTrou.get(texteTrou.size()-1).setMotATrouver(extraction) ;
						texteReponse.get(texteReponse.size()-1).setMotATrouver(tab[i].substring(0,premierPourcent)) ;
						
					} else {
						texteReponse.get(texteReponse.size()-1).setMotATrouver(tab[i]) ;
					}
					//System.out.println("Texte : "+ texteReponce.get(i).getTexte()) ;
					//System.out.println("Mot a trouver : " + texteReponce.get(i).getMotATrouver());
				}


			}
			//Ajout des null � la fin 
			texteReponse.get(texteReponse.size()-1).setMotATrouver(null)  ;
			texteTrou.get(texteTrou.size()-1).setMotATrouver(null) ;

			//Affichage dans la console
			/*for(int i=0; i<texteReponse.size() ; i++) {
				System.out.println("i = "+i+" texteReponse : Texte : "+ texteReponse.get(i).getTexte()) ;
				System.out.println("i = "+i+" texteReponse : Mot a trouver : " + texteReponse.get(i).getMotATrouver());
			}

			for(int i=0; i<texteTrou.size() ; i++) {
				System.out.println("i = "+i+" texteTrou : Texte : "+ texteTrou.get(i).getTexte()) ;
				System.out.println("i = "+i+" texteTrou : Mot a trouver : " + texteTrou.get(i).getMotATrouver());
			}*/



		}	
		in.close();		
	}

	
	

	@Override

public void enregistrer(String nomFichier) throws IOException {
		System.out.println("Enregistrement : "+nomFichier+".trou");
		ModeleTexteTrou.recuperationTexte();
		
		BufferedWriter out;

		out = new BufferedWriter(new FileWriter(nomFichier+".trou"));
		out.write("AlreadyStart") ;
		out.newLine() ;
		
		for(int i=0 ; i < texteReponse.size(); i++) {
			String motATrouver = texteReponse.get(i).getMotATrouver() ;
			String motTape = texteTrou.get(i).getMotATrouver() ;
			System.out.println(motTape);
			if(motATrouver==null) {
				motATrouver="" ;
			} else if (motATrouver.indexOf("%")!=-1) {
				motATrouver ="="+motATrouver.substring(0, motATrouver.indexOf("%")) ; 
			} else {
				motATrouver = "="+motATrouver ;
			}
			
			if(motTape==null) {
				motTape="";
			} else {
				motATrouver = motATrouver+"%"+motTape+"%=" ;
			}
			out.write(texteReponse.get(i).getTexte()+motATrouver) ;
			
			if(texteReponse.get(i).getMotATrouver()==null) {
				out.newLine() ;
			}
			
		}
	
		out.close() ;
	}


	@Override
	public boolean check() {
		ModeleTexteTrou.recuperationTexte();
		for(int i=0;i<texteReponse.size();i++){
			if(texteTrou.get(i).getMotATrouver()!=null){
			if(texteTrou.get(i).getMotATrouver().equals(texteReponse.get(i).getMotATrouver())){}
			else return false;
			}
		}
		return true;
	}
	
}
