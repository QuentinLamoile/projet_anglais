package Learning.Object;

public class Texte {
	private String texte;
	private String mot;
	
	public Texte(String texte, String mot) {
		this.texte = texte;
		this.mot = mot;
	}

	public String getTexte() {
		return texte;
	}

	public void setTexte(String texte) {
		this.texte = texte;
	}

	public String getMotATrouver() {
		return mot;
	}

	public void setMotATrouver(String mot) {
		this.mot = mot;
	}
	
}
