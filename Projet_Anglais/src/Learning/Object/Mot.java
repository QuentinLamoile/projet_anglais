package Learning.Object;

public class Mot {
	private String motAnglais;
	private String motFrancais;
	
	public Mot(String motAnglais, String motFrancais) {
		this.motAnglais = motAnglais;
		this.motFrancais = motFrancais;
	}

	public String getMotAnglais() {
		return motAnglais;
	}

	public void setMotAnglais(String motAnglais) {
		this.motAnglais = motAnglais;
	}

	public String getMotFrancais() {
		return motFrancais;
	}

	public void setMotFrancais(String motFrancais) {
		this.motFrancais = motFrancais;
	}
	

}
