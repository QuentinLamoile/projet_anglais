package Learning;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import Learning.Object.Mot;
import Loader.RessourceLoader;
import Vue.ExVoca.ModeleVoca;

public class ExerciceVoca implements LMachine {

	public static ArrayList<Mot> listMot = new ArrayList<Mot>(); //Liste compl�te des mots anglais et fran�ais Mot(anglais, francais)
	public static ArrayList<Mot> listMotTrou = new ArrayList<Mot>(); //Liste mot avec un trou al�atoire sur chaque ligne
	public static ArrayList<Mot> listMotTrouRemplissage = new ArrayList<Mot>(); //Utilis� pour la vue

	@Override
	public void ouvrir(String nomFichier) throws IOException {
		System.out.println("Ouverture ExerciceVoca");
		System.out.println(nomFichier);
		listMotTrou.clear() ; //Nettoyage de l'ArrayList � chaque ouverture de fichier
		listMot.clear() ; //Nettoyage de l'ArrayList � chaque ouverture de fichier
		listMotTrouRemplissage.clear();//Nettoyage de l'ArrayList � chaque ouverture de fichier

		//Ouverture fichier et on stocke les mots dans deux ArrayList (mot de la forme anglais==francais)
		//BufferedReader in;
		//in = new BufferedReader(new FileReader(nomFichier));
		Scanner in = new Scanner(RessourceLoader.load(nomFichier));
		String line;
		boolean DejaCommence = false ;
		
		line = in.nextLine() ;
		System.out.println(line);
		if(line.equals("AlreadyStart"))  {
			System.out.println("Fichier deja commenc�");
			DejaCommence = true ;
		}
		
		while(in.hasNextLine()){
			line = in.nextLine();
		
			if(!DejaCommence) {
				String temp[] = line.replaceAll("\\s+", " ").split("==") ;
				listMot.add(new Mot(temp[0], temp[1]));

			} else {
				int pos = line.indexOf(":") ;
				int pos1 = line.indexOf(":",pos+1) ;
				String reponseUtilisateur = line.substring(pos+1, pos1) ;
				int posEgal = line.indexOf("==") ;
				System.out.println("Fichier d�j� commenc�");
				//if(line.indexOf(reponseUtilisateur) < posEgal) {
				if(pos < posEgal) {
					//Mot depart est en francais
					String temp[] = line.replaceAll("\\s+", " ").split("==") ;
					listMotTrou.add(new Mot("", temp[1]));
					listMotTrouRemplissage.add(new Mot(reponseUtilisateur, temp[1])) ;
					line = line.replaceAll(":"+reponseUtilisateur+":","") ;
					//line = line.substring(0, line.indexOf("$$"))+line.substring(line.indexOf("$$") + 2) ;
					//System.out.println(line);
				} else {
					//Mot depart est en anglais
					String temp[] = line.replaceAll("\\s+", " ").split("==") ;
					listMotTrou.add(new Mot(temp[0], ""));
					listMotTrouRemplissage.add(new Mot(temp[0], reponseUtilisateur)) ;
					line = line.replaceAll(":"+reponseUtilisateur+":","") ;
					//line = line.substring(0, line.indexOf("$$"))+line.substring(line.indexOf("$$") + 2) ;
					//System.out.println(line);
				}
	
				String temp[] = line.replaceAll("\\s+", " ").split("==") ;
				listMot.add(new Mot(temp[0], temp[1]));
			}
		}
		in.close();	

		//Parcours de l'ArrayList et ajout dans listMotTrou

		if(!DejaCommence) {
			for(int i=0 ; i < listMot.size() ; i++) {
				double nbreAleatoire = Math.random() ; //G�n�ration nbre al�atoire
				if(nbreAleatoire < 0.5) {
					nbreAleatoire=0 ;
				} else {
					nbreAleatoire=1 ;
				}
				//Affichage de la premi�re lettre du mot
				if(nbreAleatoire==0.0) {
					listMotTrou.add(new Mot("",listMot.get(i).getMotFrancais()));
					listMotTrouRemplissage.add(new Mot(listMot.get(i).getMotAnglais().substring(0, 1),listMot.get(i).getMotFrancais()));
				} else if (nbreAleatoire==1.0) {
					listMotTrou.add(new Mot(listMot.get(i).getMotAnglais(),""));
					listMotTrouRemplissage.add(new Mot(listMot.get(i).getMotAnglais(),listMot.get(i).getMotFrancais().substring(0, 1)));
				}
				
				//Affichage sans premi�re lettre
		/*	if(nbreAleatoire==0.0) {
				listMotTrou.add(new Mot("",listMot.get(i).getMotFrancais()));
				listMotTrouRemplissage.add(new Mot("",listMot.get(i).getMotFrancais()));
			} else if (nbreAleatoire==1.0) {
				listMotTrou.add(new Mot(listMot.get(i).getMotAnglais(),""));
				listMotTrouRemplissage.add(new Mot(listMot.get(i).getMotAnglais(),""));
			}*/
			}

			System.out.println(listMotTrou.get(0).getMotAnglais()+" : "+listMotTrou.get(0).getMotFrancais());
			System.out.println(listMotTrou.get(1).getMotAnglais()+" : "+listMotTrou.get(1).getMotFrancais());
		}/* else {
			System.out.println(listMot.get(0).getMotAnglais()+" : "+listMot.get(0).getMotFrancais());
			System.out.println(listMot.get(1).getMotAnglais()+" : "+listMot.get(1).getMotFrancais());
			
			System.out.println(listMotTrou.get(0).getMotAnglais()+" : "+listMotTrou.get(0).getMotFrancais());
			System.out.println(listMotTrou.get(1).getMotAnglais()+" : "+listMotTrou.get(1).getMotFrancais());
			
			System.out.println(listMotTrouRemplissage.get(0).getMotAnglais()+" : "+listMotTrouRemplissage.get(0).getMotFrancais());
			System.out.println(listMotTrouRemplissage.get(1).getMotAnglais()+" : "+listMotTrouRemplissage.get(1).getMotFrancais());
		}*/
		
	}





	@Override
	public void enregistrer(String nomFichier) throws IOException {
		System.out.println("Enregsitrement : "+nomFichier+".voca");
		
		BufferedWriter out;

		out = new BufferedWriter(new FileWriter(nomFichier+".voca"));
		out.write("AlreadyStart") ;
		out.newLine() ;
		
		for(int i=0 ; i < listMotTrou.size(); i++) {
			String motFR = listMotTrou.get(i).getMotFrancais();
			String motAN = listMotTrou.get(i).getMotAnglais();
			
			if(motAN.equals("")) {
				motAN = listMot.get(i).getMotAnglais()+":"+ModeleVoca.recuperationTable().get(i).getMotAnglais()+":" ;
			} else if (motFR.equals("")) {
				motFR = listMot.get(i).getMotFrancais() +":"+ModeleVoca.recuperationTable().get(i).getMotFrancais()+":" ;			
			}
			
			out.write(motAN+"=="+motFR) ;
			out.newLine() ;
		}
	
		out.close() ;
		
	}

	@Override
	public boolean check() {
		ArrayList<Mot> recup = ModeleVoca.recuperationTable();
		for(int i=0;i<recup.size();i++){
			if(recup.get(i).getMotAnglais().equals(listMot.get(i).getMotAnglais()) && recup.get(i).getMotFrancais().equals(listMot.get(i).getMotFrancais())){}
			else return false;
		}
		return true;
	}

}
