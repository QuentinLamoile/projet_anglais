package Learning;

import java.io.IOException;

public interface LMachine {
	/*
	 * Ouvrir un fichier existant afin de
	 * continuer un exercice
	 */
	public void ouvrir(String nomFichier) throws IOException;
	
	/*
	 * Enregistrer un exercice pour le 
	 * continuer par la suite
	 * param : nom du fichier sans extension
	 */
	public void enregistrer(String nomFichier) throws IOException;
	
	/*
	 * Vérification des réponces
	 */
	public boolean check();
}
